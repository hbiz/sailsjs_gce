var webpack = require('webpack');
module.exports = {
  context: __dirname + '/views/components',
  entry: [
    'webpack-dev-server/client?http://localhost:8080',
    "webpack/hot/only-dev-server",
    "./index.jsx"
  ],
  output: {
    path: __dirname + '/.tmp/public/js',
    publicPath: 'http://localhost:8080/.tmp/public/',
    filename: "bundle.js"
  },
  module:{
    loaders:[
      {test:/\.jsx$/, loaders:['react-hot','babel-loader']}
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ]
};
