/**
 * HealthCheckController
 *
 * @description :: Server-side logic for managing google compute engine managed vms Healthchecks
 * @help        :: https://cloud.google.com/appengine/docs/managed-vms/
 */

module.exports = {

  healthOk: function(req,res){
    res.status(200)
      .set('Content-Type', 'text/plain')
      .send('ok');
  }
};

