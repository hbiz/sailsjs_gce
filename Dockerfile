FROM google/nodejs

WORKDIR /app
ADD package.json /app/
ADD . /app
ENV PATH ./node_modules/.bin:$PATH

EXPOSE 8080
RUN npm install -g grunt-cli
RUN npm install -g sails
RUN npm install
RUN grunt build
ENTRYPOINT ["/nodejs/bin/npm", "start"]
