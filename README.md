# sails_gce

A starterkit for a [Sails](http://sailsjs.org) application running on [Google Compute Engine Managed VMs]( https://cloud.google.com/appengine/docs/managed-vms/). Tested on OS X 10.10.1 (14B25).

# Requirements #

* [gcloud](https://cloud.google.com/sdk/gcloud/) and a [Google Developer Console Project](https://console.developers.google.com/project)
* [boot2docker](https://docs.docker.com/installation/mac/)
* The following aliases for gcloud can be helpful during debugging:

```
#!bash

alias gcpd='gcloud preview app deploy app.yaml --verbosity debug'
alias gcpi='gcloud preview app deploy app.yaml --verbosity info'
alias gcp='gcloud preview app deploy app.yaml' 
alias gcrd='gcloud preview app run app.yaml --verbosity debug'
alias gcri='gcloud preview app run app.yaml --verbosity info'
alias gcr='gcloud preview app run app.yaml' 
```
* Adding this to your ~/.bash_profile can be helpful so that docker commands are available in new terminals:
```
#!bash

$(boot2docker shellinit 2> /dev/null)
```
* The following boot2docker alias is helpful for restarting it when you get cryptic errors in gcloud:
```
#!bash

alias brs='boot2docker stop; boot2docker start; $(boot2docker shellinit)'
```

# How to: Local Dev #


```
#!bash

sails lift
```
This will run tasks in default.js and whenever any files change, the watch task will run the configured tasks.


# (Using forever) #
make sure default.js is updated to not run any grunt tasks. For some reason, having grunt tasks run when sails lifts causes a crazy infinite loop for forever. Forever should only be used in production.
Run 
```
#!bash
sudo npm install
forever start --watch app.js
```
 and it'll lift the app and re-lift whenever a file changes (as long that file is not ignored in .foreverignore)

To have logs be output in the current window, start forever this way:

```
#!bash

alias fstl='suffix=$(date +%s);forever --watch -o /tmp/sails_$suffix.log -e /tmp/sails_$suffix.log app.js; tail /tmp/sails_$suffix.log'

```

To see whatever process is started by [forever](https://github.com/foreverjs/forever):

```
#!bash

forever list
```

To stop a given process:

```
#!bash

forever stop <id>
```

ex:

```
#!bash

>forever list
info:    Forever processes running
data:        uid  command             script forever pid id logfile                    uptime       
data:    [0] db36 /usr/local/bin/node app.js 522     563    /Users/a/.forever/db36.log 0:0:0:10.172 

>forever stop db36
```

Sometimes the google dev server crashes and locks up port 8080, here's a fix:

find things running on port 8080:


```
#!bash

sudo lsof -i :8080
then kill the process by process id using:
kill -9 <processId>
```