var React = require('react');
var Chart = require('react-google-charts').Chart;

export default React.createClass({
    render() {
        return (
            <div>
                <Chart graph_id="LineChart" chartType="LineChart" width={"600px"} height={"400px"} options = {
                {title : 'twitter', curveType:'function', colors:['blue','red','darkgreen']}} data={ [
                    ['month', 'shares', 'fails','successes'],
                    ['January',2,0,2], ['February',7,1,6], ['March',30,7,23], ['April',22,2,20], ['May',23,17,6], ['Jun',5,5,0], ['July',10,2,8], ['August',15,5,10], ['September',23,3,20], ['October',18,2,16], ['November',25,20,5], ['December',70,0,70]] }
                />

                <Chart graph_id="BarChart" chartType="BarChart" width={"600px"} height={"400px"} options = {
                {title : 'twitter bars', colors:['blue','red','darkgreen']}} data={ [
                    ['month', 'shares', 'fails','successes'],
                    ['January',2,0,2],
                    ['February',7,1,6],
                    ['March',30,7,23],
                    ['April',22,2,20],
                    ['Jun',5,5,0],
                    ['July',10,2,8],
                    ['August',15,5,10],
                    ['September',23,3,20],
                    ['October',18,2,16],
                    ['November',25,20,5],
                    ['December',70,0,70]] }
                />
            </div>
        );
    }
});
